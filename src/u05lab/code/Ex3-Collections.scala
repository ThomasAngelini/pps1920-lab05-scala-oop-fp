package u05lab.code

import java.util.concurrent.TimeUnit

import scala.collection.mutable
import scala.collection.mutable.{ArrayBuffer, ListBuffer}
import scala.concurrent.duration.FiniteDuration

object PerformanceUtils {
  case class MeasurementResults[T](result: T, duration: FiniteDuration) extends Ordered[MeasurementResults[_]] {
    override def compare(that: MeasurementResults[_]): Int = duration.toNanos.compareTo(that.duration.toNanos)
  }

  def measure[T](msg: String)(expr: => T): MeasurementResults[T] = {
    val startTime = System.nanoTime()
    val res = expr
    val duration = FiniteDuration(System.nanoTime()-startTime, TimeUnit.NANOSECONDS)
    if(!msg.isEmpty) println(msg + " -- " + duration.toNanos + " nanos; " + duration.toMillis + "ms")
    MeasurementResults(res, duration)
  }

  def measure[T](expr: => T): MeasurementResults[T] = measure("")(expr)
}

import scala.collection.immutable._

object CollectionsTest extends App {

  /* Comparison */
  import PerformanceUtils._
  /*val lst = (1 to 1000000).toList
  val vec = (1 to 1000000).toVector*/

  /* Linear sequences: List, ListBuffer */
  /*val listBuffer = ListBuffer(1 to 1000000)
  val list = List(1 to 1000000)*/

  /* Indexed sequences: Vector, Array, ArrayBuffer */
  val vector = Vector(1 to 1000000)
  val array = Array(1 to 1000000)
  val arrayBuffer = ArrayBuffer(1 to 1000000)

  /* Sets */
  /*val set = Set(1 to 1000000)
  val hashSet = HashSet(1 to 1000000)
  val bitSet = ListSet(1 to 1000000)
  val sortedSet = SortedSet(1 to 1000000)
  val treeSet = TreeSet(1 to 1000000)
  val linkedHashSet = mutable.LinkedHashSet(1 to 1000000)*/

  /* Maps */
  /*var map: Map[Int, Int] = Map()
  var intMap: SortedMap[Int, Int] = SortedMap()
  var treeMap: TreeMap[Int, Int] = TreeMap()
  var listMap: ListMap[Int, Int] = ListMap()
  val anyRefMap: mutable.AnyRefMap[Int, Int] = new mutable.AnyRefMap[Int, Int]()
  val linkedHashMap: mutable.LinkedHashMap[Int, Int] = new mutable.LinkedHashMap[Int, Int]()
  val openHashMap: mutable.WeakHashMap[Int, Int] = new mutable.WeakHashMap[Int, Int]()

  for (i<- 1 to 1000000){
    map = map + (i->i)
    intMap = intMap + (i->i)
    treeMap = treeMap + (i->i)
    listMap = listMap + (i->i)
    anyRefMap.+(i->i)
    linkedHashMap.+(i->i)
    openHashMap.+(i->i)
  }*/

  assert( measure("array last"){ array.last } > measure("arrayBuffer last"){ arrayBuffer.last } )


}