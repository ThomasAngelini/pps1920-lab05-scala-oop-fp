package u05lab.code


import u05lab.code.ExamsManagerTest.ExamResult.Kind


object ExamsManagerTest extends App {

  // Possible Results
  trait ExamResultFactory {

    def failed: ExamResult

    def retired: ExamResult

    def succeededCumLaude: ExamResult

    def succeeded(valuation: Int): ExamResult
  }

  /* See: https://bitbucket.org/mviroli/oop2018-esami/src/master/a01b/e1/Test.java */

  // Single Result
  sealed trait ExamResult {

    def getKind: Kind.Value

    def getEvaluation: Option[Int]

    def cumLaude: Boolean
  }

  // Manager
  trait ExamsManager {

    def createNewCall(call: String)

    def addStudentResult(call: String, student: String, result: ExamResult)

    def getAllStudentsFromCall(call: String): Set[String]

    def getEvaluationsMapFromCall(call: String): Map[String, Int]

    def getResultsMapFromStudent(student: String): Map[String, String]

    def getBestResultFromStudent(student: String): Option[Int]

  }

  object ExamResult {

    object Kind extends Enumeration {
      val RETIRED, FAILED, SUCCEEDED = Value
    }

    def apply(kind: Kind.Value,
              laude: Boolean,
              evaluation: Option[Int] = Option.empty): ExamResult =
      ExamResultImpl(kind, evaluation, laude)

    case class ExamResultImpl(kind: Kind.Value,
                              evaluation: Option[Int] = Option.empty,
                              laude: Boolean) extends ExamResult {

      override def getKind: Kind.Value = kind

      override def getEvaluation: Option[Int] = evaluation

      override def cumLaude: Boolean = laude

      override def toString: String = this.kind match {
        case Kind.SUCCEEDED if this.cumLaude => this.kind.toString+"("+this.evaluation.get+"L)"
        case Kind.SUCCEEDED => this.kind.toString+"("+this.evaluation.get+")"
        case _ => this.kind.toString
      }

/*      override def toString: String = {
        val laudeString = if (cumLaude) "L" else ""
        val evaluationString = if (this.evaluation.isPresent) evaluation.get.toString else ""
        kind.toString + (if (!evaluationString.isEmpty) s"(${evaluationString}${laudeString})")
      }*/

    }

    import Kind._
    implicit def ordering: Ordering[Option[Int]] = (x: Option[Int], y: Option[Int]) => {
      if (x.isEmpty)
        -1 // take y
      else if (y.isEmpty)
        1 // take x
      else
        x.get compareTo y.get // take the greatest
    }

    class ExamResultFactoryImpl() extends ExamResultFactory {

      override def failed: ExamResult = ExamResult(FAILED,  laude = false)

      override def retired: ExamResult = ExamResult(RETIRED, laude = false)

      override def succeededCumLaude: ExamResult = ExamResult(SUCCEEDED, laude = true, Option(30))

      override def succeeded(valuation: Int): ExamResult = checkValuationCompatible(valuation)

      private def checkValuationCompatible(valuation: Int): ExamResult = {
        if (valuation > 30 || valuation <18)
          throw new IllegalArgumentException()
        ExamResult(SUCCEEDED, laude = false, Option(valuation))
      }
    }

    class ExamsManagerImpl() extends ExamsManager {

      import scala.collection.immutable._

      var mapExam: Map[String, Map[String, ExamResult]] = Map()

      private def checkDate(stateToAvoid: Boolean): Unit = {
        if (stateToAvoid)
          throw new IllegalArgumentException
      }

      override def createNewCall(call: String): Unit = {
        checkDate(mapExam.contains(call))
        mapExam = mapExam.+(call -> new HashMap[String, ExamResult]())
      }

      override def addStudentResult(call: String, student: String, result: ExamResult): Unit = {
        checkDate(!mapExam.contains(call))
        checkDate(mapExam(call) contains student)
        mapExam = mapExam.+(call -> mapExam(call).+(student -> result))
      }

      override def getAllStudentsFromCall(call: String): Set[String] = {
        checkDate(!mapExam.contains(call))
        mapExam(call).keySet
      }

      // Studente, val
      override def getEvaluationsMapFromCall(call: String): Map[String, Int] = {
        checkDate(!mapExam.contains(call))
        mapExam(call).collect{case (stud, exam) if exam.getEvaluation.isDefined => (stud, exam.getEvaluation.get)}
      }

      // CALL - RESULT (String Form)
      override def getResultsMapFromStudent(student: String): Map[String, String] = {

        /*
        mapExam.filter(_._2.isDefinedAt(student))
          .map{case (k, v) => (k, v(student).toString)}*/

        mapExam.collect{case (call, stud) if stud.isDefinedAt(student) => (call, stud(student).toString)}

      }

      override def getBestResultFromStudent(student: String): Option[Int] = {

        mapExam.collect{case call if call._2.contains(student) => call._2(student).getEvaluation}.max
      }
    }

  }


}