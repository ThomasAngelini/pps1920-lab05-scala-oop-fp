package u05lab.code

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions._

import u04lab.code.{Cons, List, ListImplementation, Nil}


class SomeTest {

  var l: List[Int] = new ListImplementation[Int] {}
  l = 10 :: 20 :: 30 :: 40 :: Nil()

  @Test
  def testZipRight(): Unit ={
    assertEquals(scala.collection.immutable.List((10,0), (20,1), (30,2), (40,3)), l.zipRight.toSeq)
  }

  @Test
  def partitionTest(): Unit ={
    assertEquals((Cons(20,Cons(30,Cons(40,Nil()))), Cons(10,Nil())),l.partition(_>15))
  }

  @Test
  def spanTest(): Unit ={
    assertEquals(( Nil(), Cons(10,Cons(20,Cons(30,Cons(40,Nil())))) ), l.span(_>15))
    assertEquals(( Cons(10,Nil()), Cons(20,Cons(30,Cons(40,Nil()))) ), l.span(_<15))
  }

  @Test
  def testReduce(): Unit ={
    assertEquals(100,l.reduce(_+_))
  }

  @Test
  def testTakeRight(): Unit ={
    assertEquals( Cons(30,Cons(40,Nil())),l.takeRight(2))
  }

  @Test
  def testCollect(): Unit ={
    assertEquals(Cons(9, Cons(39, Nil())),l.collect{ case x if x<15 || x>35 => x-1 })
  }

}